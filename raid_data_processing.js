var data_container = require('./raid_data_container.js')

// Local Functions
function check_member_exists(arguments, receivedMessage) {
    receivedMessage.channel.send("Kweh!")
}

// Exported Functions
function update_job_preferences(arguments, receivedMessage) {
    let discord_name = receivedMessage.member.user.tag
    if(!player_exists(discord_name)) {
        add_new_member_no_char_name(discord_name)
    }

    var job_preferences = new Map()
    for(var i = 0; i < arguments.length; ++i) {
        job_preferences.set(i, arguments[i])
    }

    data_container.job_preferences.set(discord_name, job_preferences)
}

function update_days_available(arguments, receivedMessage) {

}

function update_discord_preference(arguments, receivedMessage) {

}

function update_reserve_preference(arguments, receivedMessage) {

}

function player_exists(discord_name) {
    return data_container.members.has(discord_name)
}

function has_char_name(discord_name) {
    if(player_exists(discord_name)) {
        return data_container.members[discord_name] != ""
    }
    return false
}

function add_new_member(discord_name, char_name) {
    if(!player_exists(discord_name)) {
        data_container.members.set(discord_name, char_name)
    }
}

function add_new_member_no_char_name(discord_name) {
    add_new_member(discord_name, "")
}

function update_character_name(arguments, receivedMessage) {
    let char_name = arguments[0]
    if(arguments.length == 2) {
        char_name += " " + arguments[1]
    }

    let discord_name = receivedMessage.member.user.tag

    if(!player_exists(discord_name)) {
        add_new_member(discord_name, char_name)
    } 
    else {
        data_container.members[discord_name] = char_name
    }
}

exports.update_job_preferences = update_job_preferences
exports.update_days_available = update_days_available
exports.update_discord_preference = update_discord_preference
exports.update_reserve_preference = update_reserve_preference
exports.update_character_name = update_character_name