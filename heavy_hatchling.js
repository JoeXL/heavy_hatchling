const Discord = require('discord.js')
const client = new Discord.Client()

const commands = require('./commands.js')

client.on('message', (receivedMessage) => {
    if (receivedMessage.author == client.user) { // Prevent bot from responding to its own messages
        return
    }
    
    if (receivedMessage.content.startsWith(commands.command_prefix)) {
        processCommand(receivedMessage)
    }
})

function processCommand(receivedMessage) {
    let fullCommand = receivedMessage.content.substr(commands.command_prefix.length) // Remove the leading exclamation mark
    let splitCommand = fullCommand.split(" ") // Split the message up in to pieces for each space
    let primaryCommand = splitCommand[1] // The first word directly after the exclamation is the command
    let arguments = splitCommand.slice(2) // All other words are arguments/parameters/options for the command

    console.log("Command received: " + primaryCommand)
    console.log("Arguments: " + arguments) // There may not be any arguments

    var command_function = commands.commands_list[primaryCommand]
    if(command_function) command_function(arguments, receivedMessage)
}

client.login("NjAxMDkyODIxMTcyMDkyOTY1.XS9SFw.Y6oZSKSjT9razfRQVvhRTvBQuqY") // Replace XXXXX with your bot token