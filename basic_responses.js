
function respond_pet(arguments, receivedMessage) {
    receivedMessage.channel.send("Kweh!")
}

function respond_help(arguments, receivedMessage) {
    if (arguments.length > 0) {
        receivedMessage.channel.send("It looks like you might need help with " + arguments)
    } else {
        receivedMessage.channel.send("I'm not sure what you need help with. Try `!help [topic]`")
    }
}

exports.respond_pet = respond_pet
exports.respond_help = respond_help