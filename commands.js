const basic_responses = require("./basic_responses.js")
const raid_data_processing = require("./raid_data_processing.js")

const local_command_prefix = "!HH"

const local_commands_list = {
    "pet" : basic_responses.respond_pet,
    "help" : basic_responses.respond_help,
    "setCharName" : raid_data_processing.update_character_name,
    "setJobPreferences" : raid_data_processing.update_job_preferences
}

exports.commands_list = local_commands_list
exports.command_prefix = local_command_prefix