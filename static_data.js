var job_names = {
    "WAR" : ["war", "warrior"],
    "PLD" : ["pld", "paladin", "pala"],
    "DRK" : ["drk", "dark knight", "dark"],
    "GNB" : ["gnb", "gun breaker"],
    "SCH" : ["sch", "scholar"],
    "WHM" : ["whm", "white mage", "white"],
    "AST" : ["ast", "astro", "astrologian"],
    "DRG" : ["drg", "dragoon"],
    "MNK" : ["mnk", "monk"],
    "NIN" : ["nin", "ninja"],
    "BRD" : ["brd", "bard"],
    "BLM" : ["blm", "black mage", "black"],
    "SMN" : ["smn", "summoner"],
    "MCH" : ["mch", "machinist"],
    "SAM" : ["sam", "samurai"],
    "RDM" : ["rdm", "red mage", "red"],
    "DNC" : ["dnc", "dancer",]
}

var job_types = [
    "tank",
    "healer",
    "melee dps",
    "ranged physical dps",
    "ranged magical dps"
]

var war =  {
    names: job_names["WAR"],
    role: job_types[0]
}

var pld = {
    names: job_names["PLD"],
    role: job_types[0]
}

var drk = {
    names: job_names["DRK"],
    role: job_types[0]
}

var gnb = {
    names: job_names["GNB"],
    role: job_types[0]
}