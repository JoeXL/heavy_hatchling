#Heavy Hatchling Bot for Discord

## TODO
### Post Raid Event
#### Values (*optional)
Name, Instance, Time, *Team, *Intention
### Sign-up To Raid Event
Could be done via adding a reaction to the post event
React with the job you intend to play?
### Set Discord Preference
### Set Reserve Preference
### Set weekly date availability
### Set specific date availability
### See who is available on a given day
### Post reminder before raid start
Could tag people taking part in a raid, then @ them in the reminder
### Post update to all members in raid
If they had an @ tag then this wouldn't even need to be a command
### Pin current Events to a certain channel